package es.pue.ocajp8.javapp;

//Region de importaciones (4)
//----------------------------------------------------
//1.- Importaciones de clases: x.y.z.Class ==> Class
// A) Importaciones específicas de clases
import java.lang.System;
import java.lang.String;
import java.lang.Math;
import java.util.Random;
import java.util.Date;
import java.extensions.MyString;
import javax.extensions.MyMath;
import __.$$1.$2.$$$_$$$.persistence.$1;
//import __.$$1.$2.$$$_$$$.persistence.__;
//error: __ is not public in __.$$1.$2.$$$_$$$.persistence; cannot be accessed
//from outside package
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;


// B) Importaciones genéricas de clases
import java.util.*;
import java.sql.*;
import java.io.*;
import java.net.*;
import java.time.*;
import java.text.*;

//2.- Importaciones estáticas (Static imports)
// java.lang.Math.PI ==> PI
// A.- Importaciones estáticas específicas
import static java.lang.System.out;
import static java.lang.Math.PI;   //==>PI 
import static java.lang.Math.E;    //==>E
import static java.lang.Math.pow;  //==>pow
import static java.lang.Math.sqrt; //==>sqrt
import static javax.extensions.MyMath.test;

// B.- Importaciones estáticas genéricas
import static java.lang.System.*;
import static java.lang.Math.*;
import static javax.extensions.MyMath.*;


//Fully Qualified Name: es.pue.ocajp8.javapp.Application
class Application {
	
	//Inicializador estático
	static {
		System.out.println("Application Static Init: 1");
	}
	
	static {
		System.out.println("Application Static Init: 2");
	}
	
    //Driver: Main Class
	public static void main(String... args) {
		
		System.out.println("Este examen es una puta mierda!!!");
		System.out.println("PI = " + Math.PI);
		System.out.println("E = " + Math.E);
		System.out.println("pow = " + Math.pow(2,3));
		System.out.println("sqrt = " + Math.sqrt(5));
		
		javax.extensions.MyMath.test();
		MyMath.test();
		
		
		out.println("Este examen es una puta mierda!!!");
		out.println("PI = " + PI);
		out.println("E = " + E);
		out.println("pow = " + pow(2,3));
		out.println("sqrt = " + sqrt(5));
		
		test();
		
		//Trash.test();
		
		//MyString.$1();
		//Exception in thread "main" java.lang.SecurityException: Prohibited package name: java.extensions
        //at java.lang.ClassLoader.preDefineClass(Unknown Source)
        //at es.pue.ocajp8.javapp.Application.main(Application.java:37)
		
		new Passanger();
		new Passanger();
		new Passanger();
		new Passanger();
		
		System.out.println(StringUtils.capitalize("jordi"));
		System.out.println(WordUtils.capitalize("jordi ariño santos"));
		
		
	}  

    public static void test() {
		System.out.println("hola");
	}	
	
	static {
		System.out.println("Application Static Init: 3");
	}
	  
}







